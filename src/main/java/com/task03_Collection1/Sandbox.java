package com.task03_Collection1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Sandbox {
    private static Random random = new Random();
    public static void main(String[] args) {
        ArrayList<Pair> list = new ArrayList<Pair>();
        for(int i = 0; i < 10; i++)
            list.add(new Pair(generate(), generate()));
        Collections.sort(list);
        for(Pair pair : list)
            System.out.println(pair);
        int index = Collections.binarySearch(list, list.get(3));
        System.out.println(index + " should equal 3");
    }

    public static String generate(){
        int n = random.nextInt(10) + 1;
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < n; i++)
            builder.append((char)('a' + random.nextInt(26)));
        return builder.toString();
    }
}
