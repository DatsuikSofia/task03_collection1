package com.task03_Collection1;

public class Pair implements Comparable<Pair> {
    private String first;
    private String second;

    public Pair(String first, String second){
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    public int compareTo(Pair other) {
        return first.compareTo(other.first);
        // return second.compareTo(other.second);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}
