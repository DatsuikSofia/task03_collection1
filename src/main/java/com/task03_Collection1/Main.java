package com.task03_Collection1;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>(64);
        Container container = new Container(64);
        String value = "Sophia";
        long startTime = System.nanoTime();
        for(int i = 0; i < 1024 * 1024 * 10; i++)
            list.add(value);
        long endTime = System.nanoTime();
        System.out.println((endTime - startTime) / 1e9);
        System.gc();
        startTime = System.nanoTime();
        for(int i = 0; i < 1024 * 1024 * 10; i++)
            container.add(value);
        endTime = System.nanoTime();
        System.out.println((endTime - startTime) / 1e9);
    }
}
