package com.task03_Collection1;

public class Container {
    private static final int DEFAULT_VALUE = 1024;
    private String[] arr;
    private int index;

    public Container(){
        this(DEFAULT_VALUE);
    }

    public Container(int n){
        arr = new String[n];
    }

    public void add(String value){
        if(index >= arr.length){
            String[] newArr = new String[arr.length * 2];
            System.arraycopy(arr, 0, newArr, 0, arr.length);
            arr = newArr;
        }
        arr[index++] = value;
    }

    public String get(int index){
        return arr[index];
    }
}
